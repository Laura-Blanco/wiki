import pytest  # type: ignore
import pathlib
import json
import wiki


@pytest.fixture
def client():
    wiki.app.config["TESTING"] = True

    with wiki.app.test_client() as client:
        yield client


def test_import():
    assert wiki is not None


def test_homepage(client):
    resp = client.get("/")
    assert resp.status_code == 200
    assert b"A hub of information for home coffee enthusiasts." in resp.data


def test_view_dir(client, monkeypatch):
    test_dir = pathlib.Path(__file__).parent
    test_dir = test_dir / "test_dir/"
    monkeypatch.setattr(wiki, "page_dir", test_dir)

    resp = client.get("/view/")
    assert resp.status_code == 200
    assert b"Test" in resp.data


def test_pagename(client, monkeypatch):
    test_dir = pathlib.Path(__file__).parent
    test_dir = test_dir / "test_dir/"
    monkeypatch.setattr(wiki, "page_dir", test_dir)

    resp = client.get("/view/PageName")
    assert resp.status_code == 200


def test_get_edit(client, monkeypatch):
    test_dir = pathlib.Path(__file__).parent
    test_dir = test_dir / "test_dir/"
    monkeypatch.setattr(wiki, "page_dir", test_dir)

    response = client.get("/edit/FrontPage")
    assert response.status_code == 200


def test_post_edit(client, monkeypatch):
    test_dir = pathlib.Path(__file__).parent
    test_dir = test_dir / "test_dir/"
    monkeypatch.setattr(wiki, "page_dir", test_dir)
    monkeypatch.chdir("test_dir")

    resp = client.post(
        "/edit/PageName",
        data=dict(
            content="# Wiki Main",
            editor_name="Test",
            editor_email="name@email.com",
            description="testing...",
            submit=True,
        ),
        follow_redirects=True,
    )
    assert resp.status_code == 200
    with open("pages/FrontPage.md", "r") as f:
        content = f.read()
    assert content == "Test File\n"


def test_get_history(client, monkeypatch):
    test_dir = pathlib.Path(__file__).parent
    test_dir = test_dir / "test_dir/"
    monkeypatch.setattr(wiki, "page_dir", test_dir)

    resp = client.get("/history/FrontPage")
    assert resp.status_code == 200

    resp = client.get("/history/TestFail")
    assert b"No History Has Been Found For This Page!" in resp.data


def test_get_log(client, monkeypatch):
    test_dir = pathlib.Path(__file__).parent
    test_dir = test_dir / "test_dir/"
    monkeypatch.setattr(wiki, "page_dir", test_dir)
    # monkeypatch.chdir("/test_dir/history")

    resp = client.get("/log/44a2a687f12086ea9992dc675c0bd7ade75f4581")
    assert resp.status_code == 200


# Tests markdown conversion and sanitizing integration
@pytest.mark.parametrize(
    ["markdown", "expected"],
    [
        ("# Heading level 1", "<h1>Heading level 1</h1>"),
        ("## Heading level 2", "<h2>Heading level 2</h2>"),
        ("This is a paragraph", "<p>This is a paragraph</p>"),
        (
            "This is a paragraph\n\nThis is a second paragraph",
            "<p>This is a paragraph</p>\n<p>This is a second paragraph</p>",
        ),
        (
            "[title](https://www.example.com)",
            '<p><a href="https://www.example.com">title</a></p>',
        ),
    ],
)
def test_markdown_to_html(markdown, expected):
    html = wiki.convert_markdown_to_html(markdown)
    html = wiki.sanitize_html(html)
    assert html == expected


# Tests sanitizer against malicious code
@pytest.mark.parametrize(
    ["markdown", "expected"],
    [
        (
            "<script>console.alert(Bad Code)</script>",
            "&lt;script&gt;console.alert(Bad Code)&lt;/script&gt;",
        ),
        (
            "# This is a header with malicious code"
            "<script>console.alert(Bad Code)</script>",
            "<h1>This is a header with malicious code"
            "&lt;script&gt;console.alert(Bad Code)&lt;/script&gt;</h1>",
        ),
    ],
)
def test_sanitize_html(markdown, expected):
    html = wiki.convert_markdown_to_html(markdown)
    html = wiki.sanitize_html(html)
    assert html == expected


def test_api(client, monkeypatch):
    test_dir = pathlib.Path(__file__).parent
    test_dir = test_dir / "test_dir/"
    monkeypatch.setattr(wiki, "page_dir", test_dir)

    resp = client.get("/api/v1/pages/PageName/get")
    actual = json.loads(resp.data)
    expected = {"html": "<h1>Wiki Main</h1>", "raw": "# Wiki Main", "success": True}
    assert actual == expected
