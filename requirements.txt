Flask==1.1.2
pytest==6.2.3
pytest-cov==2.11.1
markdown==3.3.4
bleach==3.3.0
mypy==0.812